;((win, doc, {h, app}) => {

	const log = x => (console.log(x), x)
	const err = e => (console.error(e), e)

	const getRandomElement = xs => 
		xs[Math.floor(Math.random() * xs.length)]

	const findIndices = guess => (c, i) =>
		guess === c	? i : null

	const not = v => x => x !== v

	const notNull = not(null)

	const PLACEHOLDER = '__'

	const notPlaceholder = not(PLACEHOLDER)

	function* guess(display, word, guesses) {
		let wLen = word.length 
		let count = wLen
		while(guesses.filter(notPlaceholder).length !== wLen && count > 0) {
			count --
			let c = yield
			if(c && c.length > 1) return c
			word
				.map(findIndices(c))
				.filter(notNull)
				.forEach(i => guesses[i] = c)
			display(guesses.join(' '))
		}
		return guesses
	}

	const check = (guesses, word) =>
		guesses
      .filter((c, i) => c === word[i])
      .length === word.length

	const createGame = (display, word, guess) => c => {
		let { value, done } = guess.next(c)
		if(Array.isArray(value) && check(value, word)) {
			display('WINNER! The word was:', value.join(''))
		} else if(typeof value === 'string' && value === word.join('')) {
			display('WINNER! The word was:', value)
		} else if(done) {
			display('GAME OVER! The word was:', word.join(''))
		}
	}

	const introText = (display, guesses) =>
		display(guesses.join(' '))

	const newGame = display => word => {	
		let initGuesses = word.map(x => x === ' ' || x === '-' ? x : PLACEHOLDER)
		introText(display, initGuesses)
		win.submit = createGame(display, word, guess(display, word, initGuesses))
		// initial yield to kick off game
		win.submit()
	}

  /*
	const view = (() => {
		const display = (state, actions) =>
			h('pre', null, state.display)

		const state = { display: [''] }	

		const actions = {
			updateDisplay: state => (...args) => ({
				display: args
			})
		}

		const _actions = app({
      view:display,
      state,
      actions
    }, doc.querySelector('#app'))
		return (...args) => _actions.updateDisplay(args)
	})()
  */

	const view = (...args) => {
		const display = doc.querySelector('#display')	
		display.innerHTML = args.join('')
	}

	/*
	const view = console.log
	*/

  fetch('dictionary.json')
    .then(response => response.json())
    .then(dictionary => word = getRandomElement(dictionary).split(''))
    .then(newGame(view))
    .catch(err)

})(window, document, hyperapp)
